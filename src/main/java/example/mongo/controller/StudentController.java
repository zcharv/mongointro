package example.mongo.controller;

import example.mongo.model.StudentModel;
import example.mongo.repository.StudentRepository;
import example.mongo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {

    @Autowired
    private StudentRepository studentRepo;

    @Autowired
    private StudentService studentService;

    @GetMapping(path = "/students")
    public List<StudentModel> getStudents() {
//        studentService.insertStudent();
        return studentRepo.findAll();
    }

    @GetMapping(path = "/getPassed")
    public StudentModel getPassed() {
        return  studentService.getPassed();
    }

    @GetMapping(path = "/oneStudent")
    public StudentModel getoneStudent() {
        return studentService.getZach();
    }

    @DeleteMapping(path = "/delete")
    public StudentModel deleteStudent() {
        return studentService.deleteStudent();
    }



}
