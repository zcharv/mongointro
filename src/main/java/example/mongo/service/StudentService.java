package example.mongo.service;

import example.mongo.model.StudentModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class StudentService {

    public StudentService() {
    }

    @Autowired
    private MongoTemplate mongoTemplate;

    public void insertStudent(){
        StudentModel student = new StudentModel();
        student.setFirstName("Zach");
        student.setLastName("Harvey");
        ArrayList<Double> grades = new ArrayList<>();
        grades.add(75.0);
        grades.add(88.2);
        grades.add(99.1);
        grades.add(83.6);
        student.setGrades(grades);
        mongoTemplate.save(student);
    }

    public StudentModel averageGrades(){
        return mongoTemplate.
    }

    public StudentModel getZach(){
        return mongoTemplate.findOne(Query.query(Criteria.where("firstName").is("Zach")),StudentModel.class);
    }

    public StudentModel getPassed(){
        return mongoTemplate.findOne(Query.query(Criteria.where("grades").gte(75)),StudentModel.class);
    }

    public StudentModel deleteStudent(){
        return mongoTemplate.findAndRemove(Query.query(Criteria.where("firsName").is("Zach")),StudentModel.class);
    }
}
