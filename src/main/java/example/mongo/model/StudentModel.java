package example.mongo.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;

@Setter @Getter
public class StudentModel {
    @Id
    private String id;
    private String firstName;
    private String lastName;
    private ArrayList<Double> grades; // 77, 86, 32 on tests and hw

}
