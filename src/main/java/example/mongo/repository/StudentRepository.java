package example.mongo.repository;

import example.mongo.model.StudentModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "studentmodel", path = "student")

public interface StudentRepository extends MongoRepository<StudentModel, String> {
}
